<div>
    <h1>SPOTIFY DEMO</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](http://www.andreemalerva.com/), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este sitio esa hecho con la ayuda de tecnologías como lo son HTML, CSS, Javascript, en conjunto con el framework React y Next JS ☕️ 🫶🏻 🛠

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de © 2023 LYAM ANDREE CRUZ MALERVA. 

```
**Se prohibe el uso de este código y sus derivantes.**
```

## Sobre el proyecto

```
Para la instalación del proyecto en netlify, se siguio las siguientes notas:

1.- Se crea un nuevo sitio en Netlify
2.- Se configura el sitio de Next Js:
    1.1- Build command: <b>next build</b>
    1.2- Publish directory <b>.next/</b>
3.- Se agrega el siguiente plugin [NEXT.JS](https://app.netlify.com/plugins/@netlify/plugin-nextjs/install?utm_source=blog&utm_medium=next-on-netlify-jl&utm_campaign=devex):
```

# Proyect
This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

# Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

# Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

# Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
