import HeaderNav from "./components/header/header";
import styles from "../../public/css/home.module.css";
import CardGridManage from './components/cards/cardGridManage';

export default function Home() {
  return (
    <main className={styles.mainContent}>
          <div className={styles.capaOne}></div>
          <div className={styles.capaTwo}></div>
          <div className={styles.content}>
            <HeaderNav></HeaderNav>
            <div className={styles.contentMidCards}>
              <h1>Good afternoon</h1>
              <CardGridManage></CardGridManage>
            </div>
          </div>
    </main>
  );
}
